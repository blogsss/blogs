package com.captton.programa;

import com.captton.blog.*;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/**
		 * Crear BD
		 */
		Bd bd = new Bd();
		
	//Creacion de 3 usuarios :
				
		Usuario usu1= new Usuario("Alberto","Fernandez",4452156,54);
		Usuario usu2= new Usuario("Mauricio","Macri", 6586315,58);
		Usuario usu3= new Usuario("Nico","Del ca�o", 4456821, 45);
		
		Email mail1 = new Email(usu1, "albertito@yahoo.com");
		Email mail2 = new Email(usu2,"macrigato@hotmail.com");
		Email mail3 = new Email(usu3,"nicoca�o@ymail.com");
		
		usu1.asignarEmail(mail1);
		usu2.asignarEmail(mail2);
		usu3.asignarEmail(mail3);
		
		bd.getUsuarios().add(usu1);
		bd.getUsuarios().add(usu2);
		bd.getUsuarios().add(usu3);
		
		/**
		 * Crear post desde usuarios
		 * 
		 */
		
		usu1.crearPost("Ayuda!", "Necesito ayuda para crear un post");
		usu2.crearPost("Yo puedo crear post muy coloridos", "Con esta herramienta de mi eclipse");
		usu3.crearPost("Hola Mundo!", "Eso, hola a todos. texto");
		
	
		bd.getPosts().addAll(usu1.getPostsFromUser());
		bd.getPosts().addAll(usu2.getPostsFromUser());
		bd.getPosts().addAll(usu3.getPostsFromUser());
		/**
		 * Crear Comentarios desde usuarios
		 */
	
		usu1.escribirComentario("Ahora te ayudo!!", usu2.getPostsFromUser().get(0));
		usu2.escribirComentario("Que bueeeeno!!!", usu3.getPostsFromUser().get(0));
		usu3.escribirComentario("Chauuuuu!!", usu1.getPostsFromUser().get(0));
		
		/**
		 * Listar Usuarios en BD
		 * 
		 */
	
		/**
		 * Listar Post y Comentarios
		 * 
		 */
		bd.recorrerUsuarios();
		bd.recorrerPost();
	
		
		
		
	}
	
	

}
