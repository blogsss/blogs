package com.captton.blog;

public class Comentario extends Post{
	public Post post;

	public Comentario(Post post) {
		super();
		this.post = post;
	}
	
	public Comentario(String texto, Post post) {
		this.contenido = texto;
		this.post = post;
	}
	
	public Comentario(String texto, Post post, Usuario usuario) {
		this.contenido = texto;
		this.post = post;
		this.usuario=usuario;
	}
	
}
