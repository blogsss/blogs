package com.captton.blog;

import java.util.ArrayList;

public class Usuario {
	
	private String nombre;
	private String apellido;
	private Email email;
	private int dni;
	private int edad;
	ArrayList <Post> posts;
	ArrayList <Comentario> comenta;
	
	public Usuario(String nombre, String apellido, Email email, int dni, int edad) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.dni = dni;
		this.edad = edad;
		posts = new ArrayList<Post>();
		comenta = new ArrayList<Comentario>();
		
		
	}public Usuario(String nombre, String apellido, int dni, int edad) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.edad = edad;
		posts = new ArrayList<Post>();
		comenta = new ArrayList<Comentario>();
		
		
	}
	
	
	public Usuario() {
		super();
	}
	
	public void crearPost(String titulo, String texto) {
		
		Post post = new Post (titulo, this, texto);
		
		
		this.posts.add(post);
	
	}
	
	
	public void eliminarComentario(Comentario comentarios) {
		this.comenta.remove(comentarios);
	}
	
	
	public void escribirComentario(String texto,Post p) {
		
		Comentario comen = new Comentario(texto,p, this);
		p.agregarComentario(comen);
		
		this.comenta.add(comen);
	}
	
	
	
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	public void asignarEmail(Email email) {
		this.email= email;
		email.setUsuario(this);
	}
	
	public ArrayList<Post> getPostsFromUser(){
		return posts;
	}
	
	
	
	
	
	
}
