package com.captton.blog;
import java.util.*;
public class Bd {
	private ArrayList<Post> posts;
	private ArrayList<Usuario> usuarios;
	public ArrayList<Post> getPosts() {
		return posts;
	}
	public void setPosts(ArrayList<Post> posts) {
		this.posts = posts;
	}
	public ArrayList<Usuario> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(ArrayList<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	public Bd() {
		super();
		posts = new ArrayList<Post>();
		usuarios = new ArrayList<Usuario>();
	}
	
	public void recorrerUsuarios() {
		System.out.println("Usuarios en este foro: ");
		for(Usuario usu : usuarios) {
			
			System.out.println(usu.getNombre());
			System.out.println(usu.getApellido());
			System.out.println(usu.getDni());
			System.out.println(usu.getEmail().getDireccion());
			System.out.println("---------------------------------");
		}
	}
	
	public void recorrerPost() {
		System.out.println("Post en este foro: ");
		for(Post post : posts) {
			System.out.println("Usuario: "+post.getUsuario().getNombre());
			System.out.println("Titulo: "+post.getTitulo());
			System.out.println(post.getContenido());
			post.recorrerComentariosYMostrar();
			System.out.println("---------------------------------");
		}
	}
	
}
