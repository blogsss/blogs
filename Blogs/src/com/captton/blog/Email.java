package com.captton.blog;

import java.util.ArrayList;

public class Email {

	private String direccion;
	private Usuario usuario;
	ArrayList <Email> correo;
	
	public Email(Usuario user, String direccion) {
		super();
		this.usuario = user;
		this.direccion = direccion;
		correo = new ArrayList<Email>();
	}

	public Email() {
		super();
	}
	
	public String consultarEmailUsuario(Email direccion, Usuario usuario) {
		
		
		
		return (" El email " +direccion+" pertenece a "+usuario);
	}

	

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}	
	 
	public void asignarUsuario(Usuario usuario) {
		this.usuario= usuario;
		usuario.setEmail(this);
	}
	
	
		
}
