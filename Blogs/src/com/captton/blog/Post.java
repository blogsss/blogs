package com.captton.blog;

import java.util.ArrayList;



public class Post {
	
	
	protected String titulo;
	
	protected Usuario usuario;
	
	protected String contenido;
	
	
	
	protected ArrayList<Comentario> comentarios;
	
	
	
	
	public Post(){
		
		super();
		this.comentarios = new ArrayList<Comentario>();
		

		
	}


	public Post(String titulo, Usuario usuario, String contenido) {
		super();
		this.titulo = titulo;
		this.contenido = contenido;
		this.usuario = usuario;
		this.comentarios = new ArrayList<Comentario>();
		
	}
	
	

	//Getters and Setters

	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public String getContenido() {
		return contenido;
	}


	public void setContenido(String contenido) {
		this.contenido = contenido;
	}



	
	
	protected void agregarComentario(Comentario com) {
		
		this.comentarios.add(com);
		
		
		}
	
	protected void eliminarComentario(Comentario com) {
		
		this.comentarios.remove(com);
	}
	
	public void recorrerComentariosYMostrar() {
		System.out.println("Los comentarios en este post son:");
		for(Comentario com : comentarios) {
			System.out.println("Autor: "+com.getUsuario().getNombre());			
			System.out.println(com.getContenido());
			System.out.println("--------------------------------------");
		}
	}
	

	
	}
